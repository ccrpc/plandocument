# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.1.19](https://gitlab.com/ccrpc/plandocument/compare/v0.1.18...v0.1.19) (2024-06-04)


### Bug Fixes

* Point the header link to the CCRPC Homepage ([c7cb25c](https://gitlab.com/ccrpc/plandocument/commit/c7cb25cad22c75bf21e111139000a55f775dcb1b))

### [0.1.18](https://gitlab.com/ccrpc/plandocument/compare/v0.1.17...v0.1.18) (2024-05-29)

### [0.1.17](https://gitlab.com/ccrpc/plandocument/compare/v0.1.16...v0.1.17) (2024-05-07)

### [0.1.16](https://gitlab.com/ccrpc/plandocument/compare/v0.1.15...v0.1.16) (2024-05-07)

### [0.1.15](https://gitlab.com/ccrpc/plandocument/compare/v0.1.14...v0.1.15) (2024-04-17)

### [0.1.14](https://gitlab.com/ccrpc/plandocument/compare/v0.1.13...v0.1.14) (2024-04-17)


### Features

* Add eyebrow menu for CUUATS homepage ([677ce54](https://gitlab.com/ccrpc/plandocument/commit/677ce54a04a1a690f8397b72a8905feb45031759))

### [0.1.13](https://gitlab.com/ccrpc/plandocument/compare/v0.1.12...v0.1.13) (2023-07-25)


### Bug Fixes

* **resource bank search:** Remove accidental '>' and change prompt style ([d24d013](https://gitlab.com/ccrpc/plandocument/commit/d24d01311ae2bd68a48f60bdab2c0c95b130fa06))

### [0.1.12](https://gitlab.com/ccrpc/plandocument/compare/v0.1.11...v0.1.12) (2023-07-25)

### [0.1.11](https://gitlab.com/ccrpc/plandocument/compare/v0.1.10...v0.1.11) (2023-07-20)


### Bug Fixes

* Remove the double image logo from footer ([5993937](https://gitlab.com/ccrpc/plandocument/commit/59939379502f659249207f11409c2966529bff0e))

### [0.1.10](https://gitlab.com/ccrpc/plandocument/compare/v0.1.9...v0.1.10) (2023-07-20)


### Features

* Add shortcodes for resource bank ([3355b85](https://gitlab.com/ccrpc/plandocument/commit/3355b85a2a30d1fa7e32bc0fa17d78a05ea4e340))

### [0.1.9](https://gitlab.com/ccrpc/plandocument/compare/v0.1.8...v0.1.9) (2023-03-15)

### [0.1.8](https://gitlab.com/ccrpc/plandocument/compare/v0.1.7...v0.1.8) (2021-05-25)


### Features

* **charts:** update @ccrpc/charts to 0.9.3 ([bc9366f](https://gitlab.com/ccrpc/plandocument/commit/bc9366f5a85080d3b3796d61a1ecbe2cfc8cb65c))
* **footer:** add support for report card embedding ([12ad069](https://gitlab.com/ccrpc/plandocument/commit/12ad06918276f49a7868bb18178e821884f90d91)), closes [#20](https://gitlab.com/ccrpc/plandocument/issues/20)
* **hugo:** add unzip to image ([4dabfc1](https://gitlab.com/ccrpc/plandocument/commit/4dabfc1b8ee49ab885f576c13c8ae1c99e498759))


### Bug Fixes

* **footer:** fix report card JS URL ([603ae78](https://gitlab.com/ccrpc/plandocument/commit/603ae78eda8c028b9db05532eedb7d7ee7d1d1bf)), closes [#21](https://gitlab.com/ccrpc/plandocument/issues/21)

### [0.1.7](https://gitlab.com/ccrpc/plandocument/compare/v0.1.6...v0.1.7) (2021-05-07)


### Bug Fixes

* **build:** fix missing build artifacts in zip archive ([10efd7b](https://gitlab.com/ccrpc/plandocument/commit/10efd7b9bc04cad5ea35143a9a206b921e51792b))

### [0.1.6](https://gitlab.com/ccrpc/plandocument/compare/v0.1.5...v0.1.6) (2021-05-07)


### Features

* add support for Google Analytics ([a24adf2](https://gitlab.com/ccrpc/plandocument/commit/a24adf24931829901d41b819cdb46f9a51d82fe8)), closes [#19](https://gitlab.com/ccrpc/plandocument/issues/19)
* **header:** add support for custom colors and logo image ([7b52c57](https://gitlab.com/ccrpc/plandocument/commit/7b52c578c0b869602668a7d7e48b30cc17793da0)), closes [#18](https://gitlab.com/ccrpc/plandocument/issues/18)
* **sidebar:** add sidebar shortcode ([4817df9](https://gitlab.com/ccrpc/plandocument/commit/4817df98834dfb7a1133cd0538ac845102ab0b88)), closes [#16](https://gitlab.com/ccrpc/plandocument/issues/16)


### Bug Fixes

* **header:** fix current section logic to work for single pages ([3ae22cb](https://gitlab.com/ccrpc/plandocument/commit/3ae22cb9b1539d914fb92812873e6def96eaaa2a)), closes [#15](https://gitlab.com/ccrpc/plandocument/issues/15)
* **header:** reduce site title font size and adjust sticky menu ([8d786be](https://gitlab.com/ccrpc/plandocument/commit/8d786be9ba56bfc9f71cc40afb5d1a07bc6d766e))

### 0.1.5 (2020-07-06)


### Bug Fixes

* **graphic list item:** use abstract by default, if set ([d8f7af8](https://gitlab.com/ccrpc/plandocument/commit/d8f7af89fa6cab0bd93f09ac0de90471b50af309)), closes [#13](https://gitlab.com/ccrpc/plandocument/issues/13)

# [0.1.4] - 2019-10-21
- Show hyperlinks URLs in print (#12)
- Fixed closed accordion sections do not print (#12)
- Fixed comment icon is positioned incorrectly next to accordion (#12)

# [0.1.3] - 2019-10-17
- Fixed template deprecations in Hugo 0.58.3.
- Added public comment functionality.
- Temporarily removed search form.
- Improved print formatting.

# [0.1.2] - 2019-08-12
- Upgraded `@ccrpc/charts` to 0.6.0
- Upgraded USWDS to 2.0.3
- Highlight current section in top navigation

# [0.1.1] - 2019-02-13
- Upgraded to USWDS 2.0.0 beta 4
- Added permalinks to headings
- Added image expand functionality
- Made side navigation sticky
- Improved header border handling

# [0.1.0] - 2018-10-25
- Initial release
